# Study note for TensorFlow 2 object detection modals

## Demo
### Detect Cars
![alt text](./demo/images/Figure_1.png "Cars")
### Cats
![alt text](./demo/images/Figure_2.png "Cats")

## Setup
This section is based on [this post](https://medium.com/@viviennediegoencarnacion/how-to-setup-tensorflow-object-detection-on-mac-a0b72fbf470a) with updated python and tensorflow versions

### Install homebrew
Follow the instruction here [https://brew.sh/](https://brew.sh/)

### Install pyenv
Because Mac come with python 2.7, but TensorFlow 2 requires python 3.4 ~ 3.6 (at 2020-05-04). So we need to use pyenv to manage python version on our machine.
`brew install pyenv`

### Install python by using pyenv
Version we need at 4/5/2020 is python 3.7.7
`pyenv install 3.7.7`
After installed, switch the defaut python to 3.7.7
`pyenv global 3.7.7`
And verify it works
```bash
pyenv version
# output
# 3.7.7 (set by /Users/<username>/.pyenv/version)
```

Optional: Add pyenv init to your shell to enable shims and autocompletion
```bash
echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bash_profile
```

### Restart terminal, then check python and pip installed
So restart terminal or shell, then check python and pip versions

```bash
python -V
# Python 3.7.7
pip -V
# 19.0.x (We just need pip been installed, we going to upgrade it shortly)
```
### Install TensorFlow
```Bash
# Requires the latest pip
pip install --upgrade pip

# Current stable release for CPU and GPU
pip install tensorflow
```
For more details visit [here https://www.tensorflow.org/install/](https://www.tensorflow.org/install/)

### Checkout tensorflow models
`git clone https://github.com/tensorflow/models`

### Install other packages
```Bash
pip install Cython
pip install pillow
pip install lxml
pip install jupyter
pip install matplotlib
pip install PyQt5
pip install numpy
```

### Install Protobuf
Protobuf is google's library for serializing structured data

Following steps is for MAC ONLY
`brew install protobuf`

Assume tensorflow models is cloned to ./models, run following to compile Protobuf
```Bash
cd ./models/research
protoc object_detection/protos/*.proto --python_out=.
# Then add libraries to PYTHONPATH only for current terminal session
export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
# Or add path to .bash_profile
# export PYTHONPATH=$PYTHONPATH:/PATH-TO/models/research:/PATH-TO/models/research/slim
```

## Run detection

`python dectection.py` will detect the objects in the 2 demo images provided by google.

To try the model using different images, replace the images in `⁨models⁩/⁨research⁩/⁨object_detection⁩/test_images`

## Try with jupyter notebook
Run `jupyter notebook`
Visit `http://localhost:8888/notebooks/object_detection_tutorial.ipynb`
